FROM centos:7
RUN yum install -y wget epel-release vim
ENV IRODS_RELEASE "4.1.12"
SHELL ["/bin/bash", "-c"]
RUN yum install -y openssl-devel python-{psutil,requests,jsonschema}
RUN yum install -y fuse-libs perl-JSON
RUN yum install -y postgresql-odbc which lsof
RUN yum install -y git tig gcc-c++ make sudo
RUN yum install -y openssl postgresql-server
RUN yum install -y authd
RUN yum install rpm-build libcurl-devel -y
RUN yum install -y tmux
COPY setup.sh irods.sql /tmp/
RUN useradd -m -s'/bin/bash' eluser
RUN usermod -aG wheel eluser
ARG passwd
ENV PASSWD "$passwd"
RUN echo "eluser:${passwd}"| chpasswd
RUN wget https://github.com/irods/irods/releases/download/${IRODS_RELEASE}/irods-${IRODS_RELEASE}.tar.gz \
    -O /tmp/irods-${IRODS_RELEASE}.tar.gz
USER eluser
WORKDIR /home/eluser
RUN tar xvzf /tmp/irods-${IRODS_RELEASE}.tar.gz 
RUN bash -x /tmp/setup.sh
