#!/bin/bash
set -e
FILE=$(find irods* -type f| grep '/packaging/build.sh')
[ -n "$FILE" ]
sed -i.orig -e 's/python-psutil/python2-psutil/' $FILE
# 1. sudo yum install -y python-devel help2man fuse-devel bzip2-devel pam-devel \
#                        libxml2-devel python-psutil libtool unixODBC-devel
# 2. set up ICAT under postgres user (see README.1st) : start server, then psql -f /tmp/irods.sql
# 3. $ cd ~/irods*/ && \
#      ./packaging/build.sh --run-in-place icat postgres && \
#      ./plugins/database/packaging/setup_irods_database.sh
